import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App.js';
import registerServiceWorker from './registerServiceWorker';
import "babel-polyfill";	//support IE browsers for ReactTable

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
