import React from 'react'
import './header.css'

export class Header extends React.Component {
	render(){
		return(
			<div className="header-container">
				<h1>FIZZ 5.0</h1>
			</div>
			);
	}
}