import React from 'react'
import {Statistic, Modal} from 'semantic-ui-react' 
import './summary.css'
import './statistic.css'

export class Summary extends React.Component{
	constructor(props){
		super(props)
		this.state={
			total:0,
			red:0,
			yellow:0,
			green:0
		}
	}

	static getDerivedStateFromProps(nextProps, prevState){
		if(nextProps.riskData[0].green === prevState.green && nextProps.riskData[0].yellow === prevState.yellow){
			return null
		}
		return {
			total: nextProps.riskData[0].green+nextProps.riskData[0].yellow+nextProps.riskData[0].red,
			green: nextProps.riskData[0].green,
			yellow: nextProps.riskData[0].yellow,
			red: nextProps.riskData[0].red
		}	
	}

	render(){
		return(
			<Modal 
			closeIcon 
			size="small" 
			dimmer='blurring' 
			trigger={
				<div className="summary-container">
					<div className="summary-header">
						<h2>Risk Summary</h2>
					</div>
				</div>
			}
			>
				<Modal.Header>Summary</Modal.Header>
				<Modal.Content>
					<Statistic.Group>
						 <Statistic>
					      <Statistic.Value>{this.state.total}</Statistic.Value>
					      <Statistic.Label>Customers</Statistic.Label>
					    </Statistic>

					    <Statistic color='red'>
					      <Statistic.Value>{this.state.red}</Statistic.Value>
					      <Statistic.Label>At high risk</Statistic.Label>
					    </Statistic>

					    <Statistic color='yellow'>
					      <Statistic.Value>{this.state.yellow}</Statistic.Value>
					      <Statistic.Label>At moderate risk</Statistic.Label>
					    </Statistic>

					    <Statistic color='green'>
					      <Statistic.Value>{this.state.green}</Statistic.Value>
					      <Statistic.Label>At low risk</Statistic.Label>
					    </Statistic>
					</Statistic.Group>
				</Modal.Content>
			</Modal>
			);
	}
}