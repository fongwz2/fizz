export function loadData() {
	//all dummy values

	//table data
	var tableData = []
	var actions=['Give Discounts','Give Coupons','Reduce Prices','Provide Services','Give free things']
	var stayingReasons=['Good Service','Cheaper Products','Fast Delivery','Never Late']
	var leavingReasons=['Bad Service','Bad Product','Slow Delivery','Faulty Product']

	for (var i=0;i<100;i++) {
		tableData.push({
			salesperson: 'test',
			customerid: i,
			score1: Math.random().toFixed(2),
			score2: Math.random().toFixed(2),
			resolved: Math.random()>0.5?true:false
		})
	}

	//adding actions to table data
	tableData.map((row) => {
		row.action = row.score1>0.8?actions[Math.floor(Math.random() * Math.floor(5))]:null
		row.reason = row.score1>0.8?leavingReasons[Math.floor(Math.random()*Math.floor(4))]:stayingReasons[Math.floor(Math.random()*Math.floor(4))]
		return row
	})

	//risk data
	var yellow= 0
	var green = 0
	var red = 0
	var percentageData = []
	//log risk numbers as a percentage
	for(i=0;i<tableData.length;i++){
		if(tableData[i].score1>0.8){red+=1}
		else if(tableData[i].score1>0.7){yellow+=1}
		else{green+=1}
	}
	percentageData.push({yellow: yellow, green: green, red: red})
	
	return [tableData, percentageData]
}