import React from 'react'
import './type.css'

export class Type extends React.Component{
	constructor(){
		super()
		this.handleS1 = this.handleS1.bind(this)
		this.handleS2 = this.handleS2.bind(this)
	}

	handleS1(){
		this.props.callBack('search')
	}

	handleS2(){
		this.props.callBack('filter')		
	}

	render(){
		return(
			<div className="selector-container">
				<div className={this.props.selected==='search'?"selector-1-selected":"selector-1"} onClick={this.handleS1}>Search</div>
				<div className={this.props.selected==='filter'?"selector-2-selected":"selector-2"} onClick={this.handleS2}>Filter</div>
			</div>
			);
	}
}