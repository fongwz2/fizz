import React from 'react'
import './field.css'

export class Field extends React.Component{
	constructor(){
		super()
		this.handleFilterChange = this.handleFilterChange.bind(this)
		this.handleROLSChange = this.handleROLSChange.bind(this)
		this.state={filterROLS: false, category: "score1", ROLS: true, ROLSCategory: "leaving"}
		this.onSearchSubmit = this.onSearchSubmit.bind(this)
		this.onFilterSubmit = this.onFilterSubmit.bind(this)
		this.onReset = this.onReset.bind(this)	
	}

	handleFilterChange(e){
		e.preventDefault()
		if(e.target.value==="score1"){
			this.setState({filterROLS: false, category: "score1"})
		} else if(e.target.value==="score2") {
			this.setState({filterROLS: false, category: "score2"})
		} else {
			this.setState({filterROLS: true, category: "reason"})
		}
	}

	handleROLSChange(e){
		e.preventDefault()
		if(e.target.value==="leaving"){
			this.setState({ROLS: true, ROLSCategory: "leaving"})
		}
		else{
			this.setState({ROLS: false, ROLSCategory: "staying"})
		}
	}

	onSearchSubmit(e){
		e.preventDefault()

		var searchValue = e.currentTarget.searchValue.value
		var searchCategory = e.currentTarget.searchCategory.value
		this.props.searchCallBack(searchCategory, searchValue)
	}

	onFilterSubmit(e){
		e.preventDefault()
		var filterCategory = e.currentTarget.filterCategory.value
		var filterValue = 0

		if(this.state.filterROLS){
			filterValue = e.currentTarget.rolCategory.value
			this.props.filterROLSCallBack(filterCategory, filterValue)
		} else {
			var filterComparator = e.currentTarget.filterComparator.value
			filterValue = e.currentTarget.filterValue.value
			this.props.filterScoreCallBack(filterCategory, filterComparator, filterValue)
		}

	}

	onReset(e){
		e.preventDefault()
		this.props.resetCallBack()
	}

	render(){
		let view;
		if(this.props.type === 'search'){
			view=(
				<div className="search-field-container">
					<div className="search-container">
						<form className="search-form" onSubmit={this.onSearchSubmit}>
							<span className="form-fixed">Search:</span>
							&nbsp;
							<input type="text" name="searchValue" />
							&emsp;
							<span className="form-fixed">Under:</span>
							&nbsp;
							<select name="searchCategory">
								<option value="salesperson">Salesperson</option>
								<option value="customerid">CustomerID</option>
								{/*<option value="reason">Reasons</option>*/}
							</select>
							&emsp;
							<button type="submit" className="fa fa-search"/>
						</form>
					</div>
					<div className="reset-container" onClick={this.onReset}>
						<span className="reset-fixed">Reset</span>				
					</div>
				</div>
				);
		} else {
			view=(
				<div className="filter-field-container">
					<div className="filter-container">
						<form className="filter-form" onSubmit={this.onFilterSubmit}>
							<span className="form-fixed">Filter:</span>
							&nbsp;
							<select name="filterCategory" onChange={this.handleFilterChange} value={this.state.category}>
								<option value="score1">Score 1</option>
								<option value="score2">Score 2</option>
								<option value="reason">Reasons</option>
							</select>
							&emsp;
							{	
								!this.state.filterROLS &&
								<select name="filterComparator">
									<option value="=">=</option>
									<option value=">">&gt;</option>
									<option value="<">&lt;</option>
									<option value=">=">&gt;=</option>
									<option value="<=">&lt;=</option>
								</select>
							}
							&emsp;
							{
								!this.state.filterROLS &&
								<input type="float" name="filterValue" />
							}
							{
								this.state.filterROLS &&
								<select name="reasonType" onChange={this.handleROLSChange} value={this.state.ROLSCategory}>
									<option value="leaving">Leaving</option>
									<option value="staying">Staying</option>
								</select>
							}
							{
								this.state.filterROLS && !this.state.ROLS &&
								<select name="rolCategory">
									<option value="Fast Delivery">Fast Delivery</option>
									<option value="Cheaper Products">Cheaper Products</option>
									<option value="Good Service">Good Service</option>
									<option value="Never Late">Never Late</option>	
								</select>							
							}
							{
								this.state.filterROLS && this.state.ROLS &&
								<select name="rolCategory">
									<option value="Bad Product">Bad Product</option>
									<option value="Faulty Product">Faulty Product</option>
									<option value="Bad Service">Bad Service</option>
									<option value="Slow Delivery">Slow Delivery</option>
								</select>
							}
							&emsp;
							<button type="submit" className="fa fa-search"/>
						</form>
					</div>
					<div className="reset-container" onClick={this.onReset}>
						<span className="reset-fixed">Reset</span>
					</div>
				</div>
				);
		}

		return(
			<div>
				{view}
			</div>
			);
	}
}