import React from 'react'
import {Type} from './type.js'
import {Field} from './field.js'

export class SearchBar extends React.Component{
	constructor(){
		super()
		this.state={type: 'search'}
		this.typeCallBack = this.typeCallBack.bind(this)
		this.searchCallBack = this.searchCallBack.bind(this)
		this.filterROLSCallBack = this.filterROLSCallBack.bind(this)
		this.filterScoreCallBack = this.filterScoreCallBack.bind(this)
		this.resetCallBack = this.resetCallBack.bind(this)
	}

	typeCallBack(newType){
		this.setState({type: newType})
	}

	searchCallBack(searchCategory, searchValue){
		this.props.callBack(this.state.type, searchCategory, null, searchValue)
	}

	filterROLSCallBack(filterCategory, filterValue){
		this.props.callBack(this.state.type, filterCategory, null, filterValue)
	}

	filterScoreCallBack(filterCategory, filterComparator, filterValue){
		this.props.callBack(this.state.type, filterCategory, filterComparator, filterValue)
	}

	resetCallBack(){
		this.props.callBack(null, null, null, null)
	}

	render(){
		return(
			<div>
				<Type callBack={this.typeCallBack} selected={this.state.type}/>
				<Field 
					type={this.state.type} 
					searchCallBack={this.searchCallBack} 
					filterROLSCallBack={this.filterROLSCallBack} 
					filterScoreCallBack={this.filterScoreCallBack}
					resetCallBack={this.resetCallBack}
				/>
			</div>
			);
	}
}