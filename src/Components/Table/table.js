import React from 'react'
import ReactTable from 'react-table'
import {Icon, Form, Button, Checkbox, Modal} from 'semantic-ui-react'
//import 'semantic-ui-css/semantic.css';
import './modal.css'
import './dimmer.css'
import './react-table.css'
import './table.css'
import './button.css'
import './reset.css'
import './checkbox.css'
import './icon.css'
import './form.css'

var operators = {
    '<': function(a, b) { return a < b },
    '<=': function(a, b) { return a <= b },
    '>': function(a, b) { return a > b },
    '>=': function(a, b) { return a >= b },
    '=': function(a, b) { return a === b },
};

export class Table extends React.Component {
	constructor(props){
		super(props)
		//setting a dummy state preserves the original data pulled
		this.state = {data:props.tableData, dummy:props.tableData.slice(0)}
	}

	static getDerivedStateFromProps(nextProps, prevState){
		var data = prevState.dummy
		var i = 0;
		if(nextProps.search !== null){
			for( i=0; i<data.length; i++ ){
				if((data[i][nextProps.search[0]].toString().toLowerCase()).indexOf(nextProps.search[1].toLowerCase()) < 0) {
					data.splice(i,1)
					i--
				}
			}
		} else if(nextProps.filter !== null){
			if(nextProps.filter[0] === "reason"){
				console.log("wassup")
			}
			for( i=0; i<data.length; i++ ){
				if(nextProps.filter[0] === "reason"){
					if(data[i][nextProps.filter[0]] !== nextProps.filter[1]){
						data.splice(i,1)
						i--
					}
				} else {
					if(!operators[nextProps.filter[1]](data[i][nextProps.filter[0]],nextProps.filter[2])){
						data.splice(i,1)
						i--
					}
				}
			}
		} else {
			data = nextProps.tableData.slice(0)
			return{
				data: data,
				dummy: data
			}	
		}
		return {
			data: data,
		}
	}

	render(){
		this.columns = [{
			Header: 'Salesperson',
			accessor: 'salesperson'
		},{
			Header: 'CustomerID',
			Cell: row=>{
				return(
					<Modal
						closeIcon
						size="tiny"
						dimmer="blurring"
						trigger={<div className="cell-customer">{row.original.customerid}</div>}
					>
						<Modal.Header><Icon name="user"/>Customer Information</Modal.Header>
						<Modal.Content>
							<div><Icon name="id card"/>CustomerID: {row.original.customerid}<br /></div>
							<Icon name="globe"/>Region: XXXXXX<br />
							<Icon name="calendar alternate outline"/>Last Transaction Date: 20XX-XX-XX
						</Modal.Content>
					</Modal>
					)
			},
			id: 'customerid'
		},{
			Header: 'Reasons',
			accessor: 'reason'
		},{
			Header: 'Score 1',
			accessor: 'score1'
		},{
			Header: 'Score 2',
			accessor: 'score2'
		},{
			Header: 'Risk',
			getProps: (state, rowInfo, column) => {
				return {
					style: {
						background: rowInfo!==undefined?rowInfo.row.score1>0.8?'red':rowInfo.row.score1>0.7?'yellow':'green':null
					}
				};
			},
			id: 'status',
			width: 50,
			resizable: false,
			sortable: false
		},{
			Header: 'Recommended Action',
			Cell: row=>{
				return (
					row.original.score1>0.8?
						<Modal closeIcon size="tiny" dimmer='blurring' trigger={<div className="cell-action">{row.original.action}</div>}>
							<Modal.Header>Suggested Action: {row.original.action}</Modal.Header>
							<Modal.Content>
								<Form>
									<Form.Field>
										<label>Resolve by:</label>
										<input type='date' />
									</Form.Field>
									<Form.Field>
										<Checkbox readOnly={false} label='Resolve Status' defaultChecked={row.original.resolved?true:false}/>
									</Form.Field>
									<Button type='submit'>Update Action</Button>
								</Form>
							</Modal.Content>
						</Modal>
					:null)
			},
			id: 'action'
		}]

		//set dummy values for height first
		return(
			<div>
				{	
					this.state.data?
					<ReactTable 
						style={{height: "40rem"}} 
						data={this.state.data} 
						columns={this.columns}
					/>:null
				}
			</div>
			);
	}
}