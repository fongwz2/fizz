import React from 'react';
import {Header} from './Header/header.js'
import {Summary} from './Header/summary.js'
import {Table} from './Table/table.js'
import {SearchBar} from './SearchBar/searchbar.js'
import {loadData} from './Loader/data.js'
import './App.css'

class App extends React.Component {
	constructor(){
		super()
		this.data = loadData()
		this.state = {tableData: this.data[0], riskData: this.data[1], search: null, filter: null};
		this.searchBarCallBack = this.searchBarCallBack.bind(this)	
	}

	searchBarCallBack(type, category, comparator, value){
		if(type === 'search'){
			this.setState({search: [category,value], filter: null})			
		} else if(type === 'filter'){		
			if(comparator !== null){
				this.setState({filter: [category, comparator, value], search: null})
			} else {
				this.setState({filter: [category, value], search: null})
			}
		} else {
			this.setState({tableData: this.data[0], riskData: this.data[1], search: null, filter: null})
		}
	}

	render() {
	    return (
	      <div className="app" >
	      	<div className="header">
	      		<Header />
	      		<Summary riskData={this.state.riskData}/>
	      	</div>
	      	<div className="body">
	      		<SearchBar callBack={this.searchBarCallBack}/>
	      		<Table tableData={this.state.tableData} search={this.state.search} filter={this.state.filter}/>
	      	</div>
	      </div>
	    );
	}
}

export default App;
